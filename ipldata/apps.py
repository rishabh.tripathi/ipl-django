from django.apps import AppConfig


class IpldataConfig(AppConfig):
    name = 'ipldata'
