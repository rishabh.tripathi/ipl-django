from django.db import models

# Create your models here.
class matches(models.Model):
    id = models.PositiveSmallIntegerField(primary_key=True)
    season = models.IntegerField()
    city = models.CharField(max_length=200)
    date = models.DateField()
    team1 = models.CharField(max_length=200)
    team2 = models.CharField(max_length=200)
    toss_winner = models.CharField(max_length=200)
    toss_decision = models.CharField(max_length=10)
    result = models.CharField(max_length=10)
    dl_applied = models.SmallIntegerField()
    winner = models.CharField(max_length=200)
    win_by_runs = models.PositiveIntegerField()
    win_by_wickets = models.PositiveSmallIntegerField()
    player_of_match = models.CharField(max_length=200)
    venue = models.CharField(max_length=200)
    umpire1 = models.CharField(max_length=200)
    umpire2 = models.CharField(max_length=200)
    objects = models.Manager()


    # class Meta:
    #     ordering =['id'] # helps in alphabetical listing. Sould be a tuple


class deliveries(models.Model):
    match_id = models.ForeignKey(matches, on_delete=models.CASCADE)
    inning = models.PositiveSmallIntegerField()
    batting_team = models.CharField(max_length=200)
    bowling_team = models.CharField(max_length=200)
    over = models.PositiveSmallIntegerField()
    ball = models.PositiveSmallIntegerField()
    batsman = models.CharField(max_length=200)
    non_striker = models.CharField(max_length=200)
    bowler = models.CharField(max_length=200)
    is_super_over = models.PositiveSmallIntegerField()
    wide_runs = models.PositiveSmallIntegerField()
    bye_runs = models.PositiveSmallIntegerField()
    legbye_runs = models.PositiveSmallIntegerField()
    noball_runs = models.PositiveSmallIntegerField()
    penalty_runs = models.PositiveSmallIntegerField()
    batsman_runs = models.PositiveSmallIntegerField()
    extra_runs = models.PositiveSmallIntegerField()
    total_runs = models.PositiveSmallIntegerField()
    player_dismissed = models.CharField(max_length=200)
    dismissal_kind = models.CharField(max_length=200)
    fielder = models.CharField(max_length=200)
    objects = models.Manager()

    # class Meta:
    #       ordering = ('match_id','over','ball',)