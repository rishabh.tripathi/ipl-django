from django.urls import path
from . import views

urlpatterns = [
    path('1',views.index, name='index'),
    path('2',views.imports, name='imports'),
    path('3',views.outputmatches, name='outputmatches'),
    path('4',views.matches_played_per_year, name='highchmatches_played_per_yearart1'),
    path('5',views.stacked_matches_won_each_team, name='stacked_matches_won_each_team'),
    path('6',views.extra_runs_2016, name='extra_runs_2016'),
    path('7',views.economical_bowlers_2015, name='economical_bowlers_2015'),
    path('8',views.story, name='story'),
    path('9',views.lastpage, name='lastpage'),
]