import csv
from django.db import models
from ipldata.models import matches, deliveries
from django.db import transaction

matches_list = []

@transaction.atomic
def insert_matches():
    with open('matches.csv', 'r') as f:
        reader = csv.DictReader(f)
        for row in reader:
            data = dict(row)
            obj = matches(
                id=data['id'],
                season=data['season'],
                city=data['city'],
                date=data['date'],
                team1=data['team1'],
                team2=data['team2'],
                toss_winner=data['toss_winner'],
                toss_decision=data['toss_decision'],
                result=data['result'],
                dl_applied=data['dl_applied'],
                winner=data['winner'],
                win_by_runs=data['win_by_runs'],
                win_by_wickets=data['win_by_wickets'],
                player_of_match=data['player_of_match'],
                venue=data['venue'],
                umpire1=data['umpire1'],
                umpire2=data['umpire2']
            )
            obj.save()


deliveries_list = []


@transaction.atomic
def insert_deliveries():
    with open('deliveries.csv', 'r') as f1:
        reader2 = csv.DictReader(f1)
        for row in reader2:
            delivery_data = dict(row)
            delivery = matches.objects.get(pk=delivery_data['match_id'])
            obj2 = deliveries(
                match_id=delivery,
                inning=delivery_data['inning'],
                batting_team=delivery_data['batting_team'],
                bowling_team=delivery_data['bowling_team'],
                over=delivery_data['over'],
                ball=delivery_data['ball'],
                batsman=delivery_data['batsman'],
                non_striker=delivery_data['non_striker'],
                bowler=delivery_data['bowler'],
                is_super_over=delivery_data['is_super_over'],
                wide_runs=delivery_data['wide_runs'],
                bye_runs=delivery_data['bye_runs'],
                legbye_runs=delivery_data['legbye_runs'],
                noball_runs=delivery_data['noball_runs'],
                penalty_runs=delivery_data['penalty_runs'],
                batsman_runs=delivery_data['batsman_runs'],
                extra_runs=delivery_data['extra_runs'],
                total_runs=delivery_data['total_runs'],
                player_dismissed=delivery_data['player_dismissed'],
                dismissal_kind=delivery_data['dismissal_kind'],
                fielder=delivery_data['fielder']
            )
            obj2.save()
