from django.shortcuts import render
from django.http import HttpResponse
import textwrap
from django.shortcuts import render_to_response
from .models import matches, deliveries
from django.db.models import Sum,Avg,Count,Subquery
from ipldata import atomic

# Create your views here.
def lastpage(request):
    return HttpResponse("This is the last page")

def index(request):
    return render_to_response("index.html")

def imports(request):
    #atomic.insert_matches()
    #atomic.insert_deliveries()
    return HttpResponse("The database is imported.")

def outputmatches(request):
    output = matches.objects.all()
    # return render_to_response("index.html")
    return render(request, 'list.html', locals())

def matches_played_per_year(request):
    query_output = list(matches.objects.values_list('season').annotate(total_count=Count('season')).order_by('season'))
    x,y=[],[]
    for i in range(len(query_output)):
        x.append(query_output[i][0])
        y.append(query_output[i][1])
    chart={
        'chart': {'type': 'column'},
        'title': {'text': 'Number of matches played per year of all the years in IPL'},
        'xAxis': {'categories':x, 'name':'something'},
        'series': [{'name':'Year', 'data':y,'color':'green'}],
    }
    
    # con={} #Python Solution
    # for i in range(636):
    #     output = list(matches.objects.all().values_list())[i][1]
    #     if output not in con:
    #         con[output] = 1
    #     else:
    #         con[output] = con[output] + 1
    
    return render(request, 'highchart1.html',context={'charts':chart})

def stacked_matches_won_each_team(request):
    x= list(matches.objects.values_list('season').distinct())
    for i in range(len(x)):
        x[i]=x[i][0]
    teams = list(matches.objects.values_list('winner').distinct())
    for i in range(len(teams)):
        teams[i]=teams[i][0]
    query_output={}
    for i in teams:
        query_output[i]= (tup2dic(list(matches.objects.filter(winner=i).values_list('season').annotate(total_wins=Count('winner')).distinct())))
        #print(tup2dic(list(matches.objects.filter(winner=i[0]).values_list('season').annotate(total_wins=Count('winner')).distinct())))
    #print(query_output)

    for i in query_output:
        for j in range(len(x)):
            if x[j] not in query_output[i]:
                query_output[i][int(x[j])]=0
    for i in query_output:
        query_output[i]=tup2dic(sort(query_output[i],0,1,1))

    y=[]
    for i in range(len(teams)):
        temp={}
        temp['name']= teams[i]
        temp['data']= list(query_output[teams[i]].values())
        y.append(temp)

    chart={
        'chart': {'type': 'column'},
        'title': {'text': 'A stacked bar chart of matches won of all teams over all the years of IPL.'},
        'plotOptions':{'series':{'stacking':'normal'}},
        'xAxis': {'categories':x, 'name':'something'},
        'series': y,
    }

    return render(request, 'highchart1.html',context={'charts':chart})
    #return HttpResponse(y)

def extra_runs_2016(request):
    # output = list(deliveries.objects.values_list('match_id','extra_runs','bowling_team'))
    # output1 = list(matches.objects.filter(season='2016').values_list('id'))
     
    output=list(deliveries.objects.filter(match_id_id__season=2016).values_list('bowling_team').annotate(total_count=Sum('extra_runs')).order_by('bowling_team'))
    #.values_list('bowling_team'))
    #.values_list('match_id_id'))
    x,y=[],[]
    for i in range(len(output)):
        x.append(output[i][0])
        y.append(output[i][1])
    chart={
        'chart': {'type': 'column'},
        'title': {'text': 'For the year 2016 plot the extra runs conceded per team'},
        'xAxis': {'categories':x, 'name':'something'},
        'series': [{'name':'Extra Runs', 'data':y,'color':'green'}],
    }

    return render(request, 'highchart1.html',context={'charts':chart})

def economical_bowlers_2015(request):

    query_output=list(deliveries.objects.filter(match_id_id__season=2015).values_list('bowler').annotate(total_count=Sum('total_runs')).annotate(total_balls=Count('total_runs')).order_by('bowler'))
    output={}
    for i in range(len(query_output)):
        #x.append(query_output[i][0])
        #y.append((query_output[i][1]*6)/query_output[i][2])
        output[query_output[i][0]]=(query_output[i][1]*6)/query_output[i][2]
    output = tup2dic(sort(output,1,0,10))
    chart={
        'chart': {'type': 'column'},
        'title': {'text': 'For the year 2015 plot the top economical bowlers.'},
        'xAxis': {'categories':list(output.keys()), 'name':'something'},
        'series': [{'name':'Runs per ball', 'data':list(output.values()),'color':'green'}],
    }
    return render(request, 'highchart1.html',context={'charts':chart})

def story(request):
    #output1 = matches.objects.all()
    # query_output=deliveries.objects.filter(match_id_id__in=Subquery(output1.values('id'))).values_list('match_id_id__venue').annotate(venue_list=Count('match_id_id__venue'))
    # query_output=deliveries.objects.filter(match_id__in=Subquery(output1.values('id'))).values('match_id_id__venue').annotate(total_run=Sum('total_runs'),stadium=Count('match_id_id__venue'))
    # print(deliveries.objects.filter(match_id__season='2016').values('match_id__venue').annotate(total_run=Sum('total_runs'),stadium=Count('match_id__venue')).query)
    query_output = list(deliveries.objects.values_list('match_id__venue').annotate(total_run=Sum('total_runs')).annotate(total_count=Count('match_id__venue')).order_by('match_id__venue'))
    output={}
    for i in range(len(query_output)):
        output[query_output[i][0]]=(query_output[i][1]/query_output[i][2])

    chart={
        'chart': {'type': 'column'},
        'title': {'text': 'Stadiums that allow scoring the most, run per ball'},
        'xAxis': {'categories':list(output.keys()), 'name':'something'},
        'series': [{'name':'Runs per ball', 'data':list(output.values()),'color':'green'}],
    }
    return render(request, 'highchart1.html',context={'charts':chart})
    #return HttpResponse(len(query_output))

#CommonFiles

def sort(file,by,slicefrom,sliceby):
    d=sorted(file.items(), key=lambda t: t[by])
    return d if (slicefrom==sliceby) else d[slicefrom:sliceby]

def tup2dic(d):
    di={}
    for i in range(len(d)):
        di[d[i][0]]=d[i][1]
    return di