from django.core.management.base import BaseCommand, CommandError
from ipldata import atomic

class Command(BaseCommand):
    def handle(self,*args,**options):
        atomic.insert_matches()
        #atomic.insert_deliveries()